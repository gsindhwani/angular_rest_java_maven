package com.example.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.example.model.Employee;
import com.example.service.EmployeeService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("EmployeeController")
public class EmployeeController {

	// not working
	EmployeeService employeeService = new EmployeeService();
	
	@Path("/employees")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEmployees(){
		Response returnValue = null;
		Gson gson = new GsonBuilder().create();
		
		List<Employee> list =  employeeService.getAllEmployees();
		System.out.println("list size: " + list.size());
		
		String value = gson.toJson(list);
		returnValue = Response.ok(value).build();
		
		return returnValue;
	}
}