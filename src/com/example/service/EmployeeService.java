package com.example.service;

import java.util.List;

import com.example.dao.EmployeeDao;
import com.example.model.Employee;

public class EmployeeService {

	private EmployeeDao employeeDao = new EmployeeDao();
	
	public List<Employee> getAllEmployees() {
		return employeeDao.getAllEmployees();
	}

}
