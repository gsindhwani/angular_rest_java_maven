package com.example.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.example.model.Employee;
import com.example.resources.HibernateUtil;

public class EmployeeDao {
	
	@SuppressWarnings("unchecked")
	public List<Employee> getAllEmployees(){
		SessionFactory sessionFactory = (SessionFactory) HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		try{
			return session.createQuery("from Employee").list();
		}finally{
			session.close();
		}
		
	}
	
}
